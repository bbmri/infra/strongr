#!/bin/sh

if [ -n "$1" ]; then
  BRANCH=$1
else
  BRANCH="master"
fi

rm -rf ./* ./.*

wget -O /tmp/strongr.zip https://gitlab.com/bbmri/infra/strongr/repository/$BRANCH/archive.zip

. /tmp/venv/bin/activate
unzip -o /tmp/strongr.zip -d /tmp
mv /tmp/strongr*/* /opt/strongr/
rm -rf /tmp/strongr*
pip install --upgrade pip setuptools
pip install -e /opt/strongr/
python setup.py sdist bdist_wheel
