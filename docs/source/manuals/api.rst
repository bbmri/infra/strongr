Using the StrongR API
*********************

StrongR has a

Starting the JSON API server
============================

The StrongR API is currently exposed as a JSON API. In order to communicate over this API you first need to start the StrongR API-server.

Setting up the StrongR API Server
=================================

The StrongR API-Server currently supports two HTTP Server backends:
* Flask
* Gunicorn

For production installations we recommend using Gunicorn. For small testing or debugging installations we recommend using the Flask backend.

Backends are selected and configured trough the StrongR config.


.. code-block:: json

    {
       "develop":{
          "restdomain":{
             "backend":"gunicorn",
             "gunicorn":{
                "bind":"0.0.0.0:8085",
                "worker_class":"gevent",
                "worker_connections":1000,
                "workers":3
             }
          }
       }
    }

.. code-block:: shell

  pip install gevent # install extra dependency needed for gevents in gunicorn to work
  strongr restdomain:startserver
