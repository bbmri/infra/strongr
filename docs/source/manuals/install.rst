Installing StrongR
******************
StrongR currently supports two cloud-adapters. The OpenNebula and MockCloud adapters.


MockCloud
=========
The mockcloud is useful for testing containers localy.

Let's clone strongr and install it. Then create a scratch directory.

.. code-block:: shell

  pip install strongr
  mkdir /scratch
  chmod 777 /scratch

Now copy over the config below to ~/.strongr/config.json.

.. code-block:: json

  {
    "defaults": {
    },
    "develop": {
      "lock": {
        "driver": "file",
        "file": {
          "path": "/tmp/strongr-locks"
        }
      },
      "cache": {
        "driver": "local"
      },
      "db": {
        "engine": {
          "url": "sqlite:////tmp/strongr.db"
        }
      },
      "clouddomain": {
        "driver": "MockCloud",
        "MockCloud": {
          "scratch": "/scratch"
        }
      },
      "logger": {
        "handlers": {
          "default": {
            "level": "DEBUG"
          }
        },
        "loggers": {
          "": {
            "level": "DEBUG"
          }
        }
      }
    }
  }

Finally, run the mockcloud.

.. code-block:: shell

  strongr r:r

OpenNebula
==========

Open Nebula install is a more advanced production installation.

Requirements:

* An Ubuntu 18.04 Server template

Spinning up the StrongR Master VM
---------------------------------
Create a new VM using a Ubuntu 18.04 Server template. Give it at least 4 GiB of memory and one CPU core.
This VM will perform a few tasks:

* Act as a StrongR Master Node
* Run some packages like Redis, MariaDB, StatsD etc.

Make sure the disk image attached to the VM is set to persisten = yes.

Then create a new VM template to be used as the StrongR worker template. Make sure the disk image attached to the VM is set to persisten = no.

Now lets install some required packages and prepare some config files.


.. code-block:: shell

  pip install strongr

  # lets create an ssh key that salt-master can use
  mkdir /etc/salt/key
  ssh-keygen -f /etc/salt/key/id_rsa -N "" -t rsa -b 2048 -q

  # now lets create a config file so that salt knows how to
  # talk to your opennebula cloud
  cat << 'EOF' > /etc/salt/cloud.providers.d/opennebula.conf
  myopennebulacloud:
    minion:
      master: {{ip adress of your salt master here}}
    xml_rpc: {{adress of RPC2 endpoint e.g. https://api.myopennebulacloud.com/RPC2}}
    user: {{your username}}
    password: {{your password}}
    private_key: /etc/salt/key/id_rsa
    driver: opennebula
  EOF

  # followed by a config file that lets salt-master know how to
  # provision our strongr workers
  cat << 'EOF' > /etc/salt/cloud.profiles.d/strongrworker.conf
  strongrworker:
    provider: myopennebulacloud
    image: strongr-worker-ubuntu
    template: strongr-worker-ubuntu
  EOF

  # set correct permissions
  chmod 600 /etc/salt/cloud.providers.d/opennebula.conf /etc/salt/cloud.profiles.d/strongrworker.conf



Now copy over the config below to ~/.strongr/config.json.

.. code-block:: json

  {
    "defaults": {
    },
    "develop": {
      "lock": {
        "driver": "file",
        "file": {
          "path": "/tmp/strongr-locks"
        }
      },
      "cache": {
        "driver": "local"
      },
      "db": {
        "engine": {
          "url": "sqlite:////tmp/strongr.db"
        }
      },
      "clouddomain": {
        "driver": "OpenNebula"
      },
      "logger": {
        "handlers": {
          "default": {
            "level": "DEBUG"
          }
        },
        "loggers": {
          "": {
            "level": "DEBUG"
          }
        }
      }
    }
  }


.. code-block:: shell

  # start strongr
  strongr r:r

